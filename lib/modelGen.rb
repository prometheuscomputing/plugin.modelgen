require 'modelGen/translator.rb'
java_import 'com.nomagic.uml2.ext.magicdraw.auxiliaryconstructs.mdmodels.Model'
RubyAction.new(:modelGen, "Generate Ruby UML Metamodel", :Generators, Package) {action :generate_uml_metamodel}
