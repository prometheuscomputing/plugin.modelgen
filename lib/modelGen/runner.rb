case $Action
when :generate_uml_metamodel
  load 'modelGen/translator.rb'
  ModelTranslator.generate_uml_metamodel($SELECTIONS)
else
  puts "INVALID ACTION: #{$Action.inspect}"
end