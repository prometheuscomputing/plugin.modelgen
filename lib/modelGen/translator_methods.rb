class ModelTranslator
  def make_stereotype(md_stereotype, make_tags = true)
    stereotype = UmlMetamodel::Stereotype.new(md_stereotype.getName, :id => md_stereotype.getID, :documentation => md_stereotype.md_documentation)
    # built_stereotypes[md_stereotype] = stereotype # this happens elsewhere because we may have a stereotype that does not belong in @built_stereotypes but instead in @md_profile_stereotypes
    props = md_stereotype.getOwnedAttribute
    # puts;puts "#{stereotype.name} -- #{props.collect{|t| t.getName}}"
    metaclass_indicators, md_tags = props.partition{|p| p.getName =~ /^base_[A-Z].+$/}
    md_tags.each do |md_tag|
      make_tag(md_tag, stereotype) if make_tags
    end
    base_classes = StereotypesHelper.getBaseClasses(md_stereotype)
    base_classes.each do |mci|
      # FIXME use MagicDraw.uml_classes
      metaclass_type = map_to_metaclass(mci, stereotype)
      stereotype.metaclasses << metaclass_type if metaclass_type
    end
    stereotype
  end

  def make_tag(md_tag, stereotype)
    # FIXME we have to initialize @type after we have processed all of the classifiers, which will happen later -MF
    tag = UmlMetamodel::Tag.new(md_tag.getName, :stereotype => stereotype, :id => md_tag.getID, :documentation => md_tag.md_documentation)
    built_tags[md_tag] = tag
    tag
  end

  def apply_stereotypes(source, element)
    md_stereotypes  = StereotypesHelper.getAllAssignedStereotypes([source]).to_a
    # puts "#{element.name} has #{md_stereotypes.count} applied stereotypes" if md_stereotypes.any?
    included_md_stereotypes, excluded_md_stereotypes = md_stereotypes.partition do |md_stereotype|
      built_stereotypes[md_stereotype] #|| md_profile_stereotypes[md_stereotype]
    end
    warn_about_excluded_stereotypes(source, excluded_md_stereotypes)
    stereotypes_on_element = {}
    included_md_stereotypes.each do |md_stereotype|
      # puts "  #{element.name}(#{element.class}) -- #{md_stereotype.getName}<>#{md_stereotype.object_id}"
      found_stereotype = built_stereotypes[md_stereotype] #|| md_profile_stereotypes[md_stereotype]
      stereotypes_on_element[md_stereotype] = found_stereotype
    end
    stereotypes_on_element.each do |md_stereotype, stereotype|
      applied_stereotype = element.apply_stereotype(stereotype)
      md_properties_with_values = {}
      md_properties = md_stereotype.getOwnedAttribute || []
      md_properties.each do |md_property| 
        tag_name = md_property.getName
        value = source.get_tag_values(md_stereotype.getName, tag_name).first
        if value
          md_properties_with_values[md_property] = value
          # puts "(#{element.class})#{element.qualified_name} -- #{stereotype.name} -- #{md_property.getName} -- #{value}"
        end
      end
      if md_properties_with_values.any?
        md_properties_with_values.each do |md_property, value|
          tag = built_tags[md_property]
          if tag.nil?
            warn "Failed to find tag #{md_property} among #{stereotype.tags.collect{|t| t.name}}"
          end
          unless stereotype.tags.include?(tag)
            warn "Failed to find #{tag.name} among #{stereotype.tags.collect{|t| t.name}}"
          end
          # NOTE: currently not supporting generation of any documentation on AppliedTags
          UmlMetamodel::AppliedTag.new(:applied_stereotype => applied_stereotype, :instance_of => tag, :value => value)
        end
      end
    end
    # puts "#{element.class}--#{element.qualified_name}-- stereotypes: #{stereotypes.collect{|s| s.name}}" if stereotypes.any?
    # puts "#{element.class}--#{element.qualified_name} -- missing stereotypes:#{excluded_md_stereotypes.collect{|s| s.getName}}" if excluded_md_stereotypes.any?
  end
  
  def warn_about_excluded_stereotypes(source, excluded_md_stereotypes)
    excluded_md_stereotypes.each do |md_stereotype| 
      next unless md_profile_stereotypes[md_stereotype]
      md_properties = md_stereotype.getOwnedAttribute || []
      if md_properties.empty?
        warn "Not including stereotype: #{md_stereotype.getQualifiedName} applied on #{source.getQualifiedName}"
      else
        md_properties.each do |md_property| 
          tag_name = md_property.getName
          value = source.get_tag_values(md_stereotype.getName, tag_name).first
          warn "Not including stereotype tag: #{md_stereotype.getQualifiedName}::#{tag_name} with value '#{value.inspect}' applied on #{source.getQualifiedName}" if value
        end
      end
    end
  end
  
  # TODO some of these are only defined due to magicdraw using them and not because they are part of the metamodel that we actually use
  def map_to_metaclass element, stereotype
    case element.getName
    when "Element"
      UmlMetamodel::Element
    when "Property"
      UmlMetamodel::Property
    when "Package"
      UmlMetamodel::Package
    when "Model"
      UmlMetamodel::Model
    when "Profile"
      UmlMetamodel::Profile
    when "Classifier"
      UmlMetamodel::Classifier
    when "Class"
      UmlMetamodel::Class
    when "Association"
      UmlMetamodel::Association
    when "Interface"
      UmlMetamodel::Interface
    when "Enumeration"
      UmlMetamodel::Enumeration
    when "EnumerationLiteral"
      UmlMetamodel::EnumerationLiteral
    when "Operation"
      # UmlMetamodel::Operation
    when "Diagram"
      # UmlMetamodel::Diagram
    when "Comment"
      # UmlMetamodel::Comment
    when "Dependency"
      # UmlMetamodel::Dependency
    when "Component"
      # UmlMetamodel::Component
    when "NamedElement"
      # UmlMetamodel::NamedElement
    when "Constraint", "Artifact", "BehavioralFeature", "Abstraction", "Usage", "InstanceSpecification"
      # Not supported
    else
      warn "Unknown metaclass for stereotype #{stereotype.name}: #{element.getName}"
    end
  end
  
  # TODO: For now, I'm commenting this method out in favor of process_package. -SD
  #       This method incorrectly creates 'Model' elements for packages and models both.
  #       This method also ignores #excluded_packages (which used to be EXCLUDED_PACKAGE_NAMES)
  # def process_model md_model, options = {}
  #   model = UmlMetamodel::Model.new(md_model.getName, :id => md_model.getID, :documentation => md_model.md_documentation)
  #   built_packages[md_model] = model
  #   child_packages = md_model.packages
  #   child_packages.each do |md_child_package|
  #     process_package(md_child_package, model)
  #   end
  #
  #   get_imported_packages_for(md_model).each do |md_imported_package|
  #     imported_package = process_package(md_imported_package)
  #     model.add_import(imported_package)
  #   end
  #   model
  # end
  
  def process_package(md_package, parent = nil, options = {})
    return nil if ModelTranslator.excluded_packages.include?(md_package) && !options[:process_umm_adapter]
    if already_existing_package = built_packages[md_package]
      umm_pkg = already_existing_package
    else
      package_type = case md_package
      when MagicDraw::ModelImpl
        UmlMetamodel::Model
      when MagicDraw::ProfileImpl
        UmlMetamodel::Profile
      when MagicDraw::PackageImpl
        UmlMetamodel::Package
      else
        raise "Don't know what to do with a #{md_package}"
      end
      umm_pkg = package_type.new(md_package.getName, :id => md_package.getID, :documentation => md_package.md_documentation)
      built_packages[md_package] = umm_pkg
    end
    
    v = md_package.get_tag_values('metainformation', 'version').first
    umm_pkg.version = v if v
    
    if parent.nil? && md_package.getOwningPackage
      # should only get here and be true if we got here via a PackageImport
      parent = process_package(md_package.getOwningPackage, nil, :parent_of_import => true)
    end
    if parent # #add will not add if it is already there
      parent.add_content(umm_pkg)
    else
      orphans[md_package] = umm_pkg
    end
  
    unless options[:parent_of_import]
      child_packages = md_package.packages
      child_packages.each do |md_child_package|
        process_package(md_child_package, umm_pkg)
      end
    end
  
    get_imported_packages_for(md_package).each do |md_imported_package|
      imported_package = process_package(md_imported_package)
      umm_pkg.add_import(imported_package)
    end
      
    umm_pkg
  end

  # Should handle anything packageable
  # Tries to find a built umm package that is the parent for each orphan. If parent package exists then the orphan is added to it.  All orphans for which there is not umm package to serve as a parent are returned.
  def process_orphans
    still_orphans = []
    orphans.each do |md_element, umm_element|
      parent_of_orphan = md_element.getOwningPackage
      umm_parent = built_packages[parent_of_orphan]
      if umm_parent
        umm_parent.add_content(umm_element)
      else
        still_orphans << umm_element
      end
    end
    still_orphans
  end

  def get_imported_packages_for(md_package)
    imported_packages = []
    md_package.getPackageImport.each do |pi|
      imp_pkg = pi.getImportedPackage
      # puts "import is #{imp_pkg.getName}"
      imported_packages << imp_pkg unless imp_pkg.getName == 'UML2 Metamodel'
    end
    imported_packages
  end

  def make_classifier(md_classifier)
    name = md_classifier.getName
    case
    when md_classifier.primitive?
      model = UmlMetamodel::Primitive.new(name)
    when md_classifier.enumeration?
      model = UmlMetamodel::Enumeration.new(name)
      md_classifier.getOwnedLiteral.each{|l| make_literal(l, model)}
    when md_classifier.datatype?
      model = UmlMetamodel::Datatype.new(name)
    when md_classifier.interface?
      model = UmlMetamodel::Interface.new(name)
    when md_classifier.class?
      model = UmlMetamodel::Class.new(name)
      model.is_singleton = md_classifier.is_singleton?
    else
      raise "Unknown classifier type -- #{md_classifier.getQualifiedName} -- #{md_classifier.inspect}"
    end
    model.id            = md_classifier.getID
    model.documentation = md_classifier.md_documentation
    model.is_abstract   = md_classifier.isAbstract
    built_classifiers[md_classifier] = model
    model
  end

  def make_literal(md_enumeration_literal, enumeration)
    # TODO someday we will support enumeration literals that are of types other than String
    literal = UmlMetamodel::EnumerationLiteral.new(md_enumeration_literal.getName, :id => md_enumeration_literal.getID, :documentation => md_enumeration_literal.md_documentation)
    enumeration.add_literal(literal)
    built_literals[md_enumeration_literal] = literal
  end
  
  def make_property(md_property)
    md_type = md_property.getType
    type    = find_type(md_type)
    name = md_property.getName
    name = nil if name.empty?
    property              = UmlMetamodel::Property.new(name, :id => md_property.getID, :documentation => md_property.md_documentation)
    property.aggregation  = md_property.getAggregation.getName.to_sym
    property.is_navigable = md_property.isNavigable
    property.is_unique    = md_property.isUnique
    property.is_ordered   = md_property.isOrdered
    property.is_derived   = md_property.isDerived
    md_default_value      = md_property.defaultValue
    # defaultValue is a MD ValueSpecification as defined here:
    # file://localhost/Applications/MagicDraw%20UML%20PE/openapi/docs/javadoc/com/nomagic/uml2/ext/magicdraw/classes/mdkernel/ValueSpecification.html
    property.default_value = 
      case md_default_value
      when NilClass, Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::LiteralNullImpl
        nil
      when Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::LiteralRealImpl
        Float(md_default_value.value)
      when Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::LiteralIntegerImpl, Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::LiteralUnlimitedNaturalImpl
        Integer(md_default_value.value)
      when Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::InstanceValueImpl
        instance = md_default_value.instance
        # Currently only handling InstanceValues for Enumeration literals
        if type.is_a?(UmlMetamodel::Enumeration)
          # TODO: This should be a reference to the literal, not the text of the literal
          instance.getName
        else
          nil
        end
      else
        md_default_value.value if md_default_value.respond_to?(:value)
      end
    property.lower      = multiplicity_min(md_property)
    property.upper      = multiplicity_max(md_property)
    
    warn "Property #{md_property.getQualifiedName} has a type that is not part of this model: #{md_type.getQualifiedName}." if md_type && type.nil?
    property.type = type if type
    
    built_properties[md_property] = property
    property
  end

  def find_type(source)
    return unless source
    type = find_classifier_by_source(source) || find_primitive_by_source(source)
  end

  def find_classifier_by_source(source)
    built_classifiers[source]
  end

  def find_primitive_by_source(source)
    # Can't just look by key because it is comparing objectID (or something).  Apparently MD will create two different objects that represent the same element.
    mapped_primitive = UmmAdapter.md_to_umm_primitive_mapping.find { |k,_| k.getQualifiedName == source.getQualifiedName }
    # HACK is this OK?  Ostensibly we couldn't find it because it wasn't part of the model.
    mapped_primitive ? mapped_primitive.last : nil
  end

  def multiplicity_min(md_property)
    cardinalize(md_property)[:min]
  end

  def multiplicity_max(md_property)
    cardinalize(md_property)[:max]
  end

  def cardinalize(md_property)
    multiplicity_string = ModelHelper.getMultiplicity(md_property)
    case multiplicity_string
    when '' # default multiplicity
      lower = 0
      upper = 1
    when '*'
      lower = 0
      upper = Float::INFINITY
    when /^\d+$/ # Single value
      lower = upper = multiplicity_string.to_i
    when /^(\d+)\.\.(\*|\d+)$/ # lower value .. (* or upper value)
      lower = $1.to_i
      upper = ($2 == '*') ? Float::INFINITY : $2.to_i
    end
    {:min => lower, :max => upper}
  end

  def make_association(md_association)
    return nil unless verify_md_association_is_part_of_model(md_association)
    owning_package = get_owning_package_for_md_association(md_association)
    return if already_existing_association = built_associations[md_association]
    args                 = {}
    args[:name]          = md_association.getName unless md_association.is_a?(MagicDraw::AssociationClass)
    md_properties        = md_association.getMemberEnd.to_a
    args[:properties]    = md_properties.collect { |md_property| built_properties[md_property] }
    args[:documentation] = md_association.md_documentation
    association          = UmlMetamodel::Association.new(args)
    if built_classifiers[md_association]
      # Then md_association is an association class
      ids = UmmAdapter.split_id(md_association.getID)
      if ids.count == 2
        # Then it was already a compound id that we can split apart
        association.id = ids.last
      else
        # Then it wasn't a compound id and we need to assign a new one
        association.id = "ac_#{md_association.getID}"
      end
    else
      association.id = md_association.getID
    end
    built_associations[md_association] = association
    owning_package.add_content association
    association.is_derived  = md_association.isDerived
    if association_class = built_classifiers[md_association]
      association.association_class = association_class
      association_class.association_class_for = association
      # We create both an association and a class that are associated to each other where OMG spec for UML has a single AssociationClass object.  Both our association and class will have the same value for #source
    end
    association
  end

  # In MagicDraw associations can apparently be owned either by packages or by classifiers (and maybe other stuff?? gosh I hope not).  Why can they be owned by classifiers?  Why not just choose one type of entity and stick with that?  The OMG spec is no help here at all.  Our metamodel takes the position that classifiers can not own associations and that only packages can.  Note: In UML Association is a specialization of Classifier.  This is not true in our metamodel.
  def get_owning_package_for_md_association(md_association)
    md_owner = md_association.getOwner
    owning_package = built_packages[md_owner]
    unless owning_package
      # we assume the association was owned by a classifier and that the classifier is owned by a package
      if md_owner = md_owner.getOwner
        owning_package = built_packages[md_owner]
      end
    end
    owning_package
  end  

  def get_all_md_properties_for(md_classifier)
    md_properties   = md_classifier.getAttribute.to_a
    md_associations = ModelHelper.associations(md_classifier) # MagicDraw really sucks...I mean, why in the #@$% do I need a @#$@#ing ModelHelper to get the associations from a classifier?
    md_properties_from_md_associations = md_associations.collect do |md_association|
      next unless verify_md_association_is_part_of_model(md_association)
      # end_one = ModelHelper.getFirstMemberEnd(md_association)  rescue md_association.getMemberEnd[0]
      # end_two = ModelHelper.getSecondMemberEnd(md_association) rescue md_association.getMemberEnd[1]
      end_one = md_association.getMemberEnd[0]
      end_two = md_association.getMemberEnd[1]
      
      if end_two.getType == end_one.getType
        # Then this is an association where both properties belong to the same class, i.e. a self-association
        [end_one, end_two]
      else
        # NOTE:  Must use getType on other_end because getOwner will not work with non-navigable ends
        end_two.getType.equal?(md_classifier) ? end_one : end_two
      end
    end.flatten.uniq.compact
    (md_properties_from_md_associations + md_properties).uniq
  end

  def verify_md_association_is_part_of_model(md_association)
    # make sure this association is in a package that is included
    # TODO log these messages
    unless get_owning_package_for_md_association md_association
      warn "Association b/w #{md_association.getMemberEnd.collect{ |prop| prop.getType.getName }} is owned by #{md_association.getOwner.getQualifiedName.inspect}, which is not part of the selected model."
      return false
    end
    md_association.getMemberEnd.each do |md_property|
      found_type = find_type(md_property.getType)
      unless found_type
        warn "Association owned by #{md_association.getOwner.getQualifiedName.inspect} which is between #{md_association.getMemberEnd.collect{ |prop| prop.getType.getName }} includes a property with a type that is not part of the selected model: #{md_property.getType.getName}."
        return false
      end
    end
    true
  end

end
