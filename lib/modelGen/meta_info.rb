# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module ModelGen
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # Required
  GEM_NAME = "plugin.modelGen"
  VERSION  = '2.3.1'
  AUTHORS  = ["Michael Faughn"]
  SUMMARY = %q{Generates Instances of the Metamodel specified by the uml_metamodel gem from MagicDraw UML}
  
  # Optional
  EMAILS      = ["m.faughn@prometheuscomputing.com"]
  DESCRIPTION = %q{Generates Instances of the Metamodel specified by the uml_metamodel gem from MagicDraw UML.}
  
  LANGUAGE         = :ruby
  LANGUAGE_VERSION = ['>= 2.3']
  RUNTIME_VERSIONS = { :jruby => ['>= 9.1'] }

  DEPENDENCIES_RUBY = { 
    :common               => '~> 1.11',
    :Foundation           => '~> 1.8',
    :magicdraw_extensions => '~> 0.13',
    :appellation          => '~> 1.0',
    :uml_metamodel        => '~> 3.1',
    :umm_adapter          => '~> 0.0',
    :rainbow              => '~> 3.0'
  }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { :magicdraw_plugin_runner => '~> 0.0' } # Goes in the MagicDraw gemset!  
end
