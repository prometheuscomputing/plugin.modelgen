require 'yaml'
# require 'Foundation/load_path_management' # for #relative
# require 'fileutils'
# include FileUtils::Verbose
require_relative 'meta_info'
load 'uml_metamodel.rb'
load 'appellation.rb'
load 'magicdraw_extensions.rb'
load 'umm_adapter.rb'
