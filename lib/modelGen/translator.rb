# helps with debugging
require 'pp'
require 'rainbow'
require 'uml_metamodel'

require 'modelGen/requirements.rb'
require 'modelGen/settings.rb'
require 'modelGen/magicdraw_specific_code.rb'
require 'modelGen/translator_methods.rb'
require 'appellation/appellation.rb'
require 'common/constants.rb'
load 'uml_metamodel/validation.rb'

class ModelTranslator
  attr_reader :built_packages        
  attr_reader :built_classifiers
  attr_reader :built_literals     
  attr_reader :built_properties      
  attr_reader :built_associations    
  attr_reader :built_stereotypes     
  attr_reader :built_tags            
  attr_reader :md_profile_stereotypes
  attr_reader :translation_warnings
  attr_reader :translation_messages
  attr_reader :options
  attr_reader :orphans
  attr_reader :translation
  attr_reader :validation_errors
  attr_reader :validation_warnings

  DEFAULT_PROJECT_VERSION = '0.0.0'
  
  def self.generate_uml_metamodel(user_selections, options = {})
    # Clear any magicdraw_extensions Warnings. These warnings are used in the case that a plugin raises an exception
    Warnings.clear

    user_selections = Array(user_selections)
    options.merge!(:user_selections => user_selections)

    translator = new(options)
    translator.translate_md_to_umm_project(options)
    # TODO It would be nice to have a selection panel for options in MagicDraw.  Easier would be just putting stereotypes on the selected model.  For now, this option is hard-coded...
    umm_opts = options.merge({:allow_untyped_properties => true})
    translator.validate_translation(umm_opts)
    unless options[:no_dsl]
      umm_opts[:no_error_checking] = true
      begin
        translator.write_dsl(umm_opts)
      rescue StandardError => e
        # STDERR.puts e
        # STDERR.puts e.backtrace
        error_msg = ["FAILURE to write DSL!"]
        error_msg << e.message
        error_msg << e.backtrace
        if translator.validation_errors.any?
          error_msg << ""
          error_msg << "DSL Validation Errors:"
          translator.validation_errors.each { |e| error_msg << (' -- ' + e) }
        end
        RubyAdapter.message = error_msg
        return
      end
    end
      
    Warnings.clear # Clear magicdraw_extensions Warnings list (the plugin did not crash if this line is reached, so they aren't needed)
    r = translator.report(umm_opts)
    RubyAdapter.message = r
    puts r if $model_importer_test
    translator.translation
  end
  
  def validate_translation(opts = {})
    eaw = translation.errors_and_warnings(opts)
    @validation_errors   = eaw[:errors].uniq
    @validation_warnings = eaw[:warnings].uniq
  end
  
  def report(opts)
    # Validation of the translated model (not to be confused with warnings generated during translation, even though there may be some overlap).
    r   = []
    if translation_warnings.any? || validation_errors.any? || validation_warnings.any?
      if translation_warnings.any?
        r << 'Translation succeeded with warnings:'
        translation_warnings.uniq.each { |w| r << " -- #{w}" }
      else
        r << 'Translation succeeded with no warnings.'
      end
      if translation_messages.any?
        r << ''
        r << 'Informational messages from translation:'
        translation_messages.uniq.each { |m| r << " -- #{m}" }
      end
      if validation_errors.any?
        r << ''
        r << 'Validation of the translated model resulted in errors:'
        validation_errors.each { |e| r << " -- #{e}" }
      end
      if validation_warnings.any?
        r << ''
        r << 'Validation of the translated model resulted in warnings:'
        validation_warnings.each { |w| r << " -- #{w}" }
      end
    else
      r << 'Success!'
    end
    r.join("\n")
  end
  
  def translate_md_to_umm_project(options = {})
    # use the model that is normally named 'Data' (unless user renamed it) if the user did not select a single model on their own.  If, for instance, the user selects one or more packages, it is implied that we should be looking at the root model for metainformation and generation options.
    us = [(@user_selections || options[:user_selections])].flatten
    selected_model = us.first if us.count == 1 && us.first.is_a?(MagicDraw::Model)
    selected_model ||= MagicDraw.root_model
    # Create a UML project wrapper, and set up options
    project = create_project(selected_model)
    # If the 'Data' model was selected, instead use contents
    if us.count == 1 && us.first == MagicDraw.root_model
      us = us.first.packages
    end
    # Construct all project contents from the selected models
    project.contents = build(us)
    @translation = project
  end
  
  def write_dsl(options = {})
    # NOTE: Prometheus is defined by magicdraw_extensions
    # Output Marshal file
    # marshal_filename = "#{project.name}#{('-' + project.version) if project.version}.marshal"
    # marshal_file = File.join(Prometheus, 'Generated_Code', marshal_filename)
    # project.to_marshal_file(marshal_file)
    # Output DSL file
    dsl_filename = "#{translation.name}#{('-' + translation.version) if (translation.version && translation.version != DEFAULT_PROJECT_VERSION)}.rb"
    # STDERR.puts "dsl_filename is #{dsl_filename.inspect}"
    dsl_file = $model_importer_test || File.join(Prometheus, 'Generated_Code', dsl_filename)
    $model_importer_test = nil
    translation.to_dsl_file(dsl_file, options)
  end
  
  def initialize(options = {})
    @user_selections        = options.delete(:user_selections)
    @built_packages         = {}
    @built_classifiers      = {}
    @built_literals         = {}
    @built_properties       = {}
    @built_associations     = {}
    @built_stereotypes      = {}
    @built_tags             = {}
    @md_profile_stereotypes = {}
    @translation_warnings   = []
    @translation_messages   = []
    @options                = options
    @orphans                = {}
  end
  
  def warn(warnings)
    [warnings].flatten.each do |w|
      # Add warning to list of warnings
      @translation_warnings << w 
    
      # Add warning to magicdraw_extensions list of warnings
      # These warnings are displayed if this plugin raises an exception and does not complete successfully.  We can clear them if we don't raise an error.  We are keeping track of the warnings in this instance of translator so that we can use them for whatever we want to later (without being concerned about magicdraw_extensions).
      Warnings << w
    end
  end

  def message(words)
    # Print message to terminal output immediately
    STDERR.puts words
    # Add message to list of messages
    @translation_messages << words
  end
    
  def build(models)
    setup_md_profile_stereotypes
    processing_options = {}
    processing_options[:process_umm_adapter] = models.count == 1 && models.first.getName == 'UmlMetamodelAdapter'
    top_level_models = models.collect { |model| process_package(model, nil, processing_options) }.compact
    # puts Rainbow(top_level_models.inspect).yellow
    top_level_models += process_orphans # this is alright here because at this point we are only handling orphaned packages with this.  Needs to be refactored if you want to handle classifiers and/or associations and/or stereotypes as well.
    top_level_models.uniq!
    # puts Rainbow(top_level_models.inspect).yellow
        
    # Define all of the stereotypes and tags
    built_packages.each do |source, package|
      # need to add documentation and applied stereotypes before other processing
      md_stereotypes = source.getOwnedStereotype
      md_stereotypes.each do |md_st|
        new_stereotype           = make_stereotype(md_st)
        built_stereotypes[md_st] = new_stereotype
        package.add_content(new_stereotype)
      end
    end
    # we will have to go back and apply stereotypes to things at the end.  Best to do that after we have more other stuff done.

    # Make all the classifiers
    built_packages.each do |source, package|
      md_classifiers = source.classes + source.datatypes + source.enumerations + source.interfaces + source.primitives
      md_classifiers.each{|md_classifier| package.add_content(make_classifier(md_classifier))}
    end

    # Handle Inheritance
    md_classifiers_with_parents_that_were_not_in_the_model = []
    built_classifiers.each do |source, classifier|
      md_parents = source.getGeneral
      parents    = md_parents.collect { |md_parent| find_type(md_parent) }
      # FIXME this could fail unless we look all the way up the chain.  Must get all parents.
      if parents.include?(nil)
        md_classifiers_with_parents_that_were_not_in_the_model << source
        warn "#{classifier.name} is being removed from the model because not all of its ancestors are part of the model"
      else
        parents.each { |parent| parent.add_child(classifier) }
      end
    end
    md_classifiers_with_parents_that_were_not_in_the_model.each do |md_bad_classifier|
      bad_classifier = built_classifiers[md_bad_classifier]
      bad_classifier.package.remove_content bad_classifier
      built_classifiers.delete md_bad_classifier
    end
    
    # Handle Interface Implementation
    built_classifiers.each do |source, classifier|
      next unless classifier.is_a?(UmlMetamodel::Class) # only Classes can implement interfaces
      implemented_interfaces = source.implemented_interfaces.collect{|i| find_type(i)}.compact
      implemented_interfaces.each{|ii| ii.add_implementor classifier}
    end
    # Build all properties
    built_classifiers.each do |source, classifier|
      md_properties = get_all_md_properties_for(source)
      md_properties.each do |md_property|
        new_property = make_property(md_property)
        if new_property
          classifier.add_property(new_property)
        else
          # if there isn't a new property it should be becaused it was typed as something that was not part of the model passed into this plugin
          if md_property.getType
            warn "(Property) #{md_property.getQualifiedName} could not be created because #{md_property.getType.getQualifiedName} is not part of the model"
          else
            warn "(Property) #{md_property.getQualifiedName} could not be created because it has no type"
          end
        end
      end
    end
    
    # Build all associations
    built_classifiers.each do |source, classifier|
      ModelHelper.associations(source).to_a.each do |md_association|
        make_association(md_association)
      end
    end
    
    # Apply Stereotypes
    [built_properties, built_associations, built_classifiers, built_packages, built_stereotypes, built_tags].each do |hash|
      hash.each {|source, element| apply_stereotypes(source, element)}
    end
    
    top_level_models
  end
  
  def create_project(selected_model)
    md_project                = MagicDraw.project
    project                   = UmlMetamodel::Project.new
    project.id                = md_project.getID
    project.original_filename = md_project.getFileName.split('/').last
    project.generated_by      = "plugin.modelGen-#{ModelGen::VERSION}"
    project.generated_at      = Time.now
    project.additional_info[:selected_model] = {:name => selected_model.getQualifiedName, :id => selected_model.getID}
    # Set version
    # First see if version is defined via the <<metainformation>> stereotype on the root model
    version   = selected_model.get_tag_value('metainformation', 'version')
    # Next see if we can find it on the legacy stereotype tag
    unless version
      version = selected_model.get_tag_value('options', 'version')
      if version
        warn "Project version was taken from the 'version' tag of the 'options' stereotype.  This usage is deprecated in favor of using the 'metainformation' stereotype defined in the UmlMetamodelAdapter profile (found in the umm_adapter git repository)."
      end
    end
    # Otherwise derive version from filename
    version ||= version_from_filename(project.original_filename)
    version ||= DEFAULT_PROJECT_VERSION # if nothing else we do this
    project.version = version
    # Set name
    # First see if name is defined via the <<metainformation>> stereotype on the root model
    name   = selected_model.get_tag_value('metainformation', 'project_name')
    # Otherwise derive name from filename
    name ||= name_from_filename(md_project.getName)
    project.name = name
    
    # Set up generation options from <<generation_options>> stereotype
    # Whether or not to pluralize generated role names
    # Defaults to true
    pluralize_role_names = selected_model.get_tag_value('generation_options', 'pluralize_role_names')
    project.additional_info[:selected_model][:pluralize_role_names] = pluralize_role_names unless pluralize_role_names.nil?
    pluralize_role_names = true if pluralize_role_names.nil?
    @options[:pluralize_role_names] = pluralize_role_names
    # Whether or not to snakecase generated role names
    # Defaults to true
    snakecase_role_names = selected_model.get_tag_value('generation_options', 'snakecase_role_names')
    project.additional_info[:selected_model][:snakecase_role_names] = snakecase_role_names unless snakecase_role_names.nil?
    snakecase_role_names = true if snakecase_role_names.nil?
    @options[:snakecase_role_names] = snakecase_role_names
    
    project
  end
end
