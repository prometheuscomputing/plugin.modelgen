# Put the project on the load path
require 'Foundation/load_path_management'
$:.unshift relative('../')

# Put local dependencies on the load path (Checks for sibling project folders)
$:.unshift relative('../../../magicdraw_plugin_runner/lib/')
$:.unshift relative('../../../magicdraw_extensions/lib/')

require 'magicdraw_plugin_runner/plugin_runner'

# Load magicdraw_extensions
# Only magic_draw_stereotypes is used by this file, but it seems to mess up the "load 'magicdraw_extensions.rb'" call later,
# so other magicdraw_extensions are required here as well
#require 'magicdraw_extensions' # <- Seems to be broken
require 'magicdraw_extensions/object_extensions'
require 'magicdraw_extensions/magic_draw_stereotypes'
require 'magicdraw_extensions/magic_draw_nav'

class ModelGenRunner < MagicdrawPluginRunner::PluginRunner
  def setup_for_project_load
    # Example of how to setup default project path or arguments
    #self.project_path ||= relative('test.mdzip')
    #self.args[0] ||= 'Test'
  end
  
  def activate_plugin(md_env = {})
    app = md_env[:app]
    project = md_env[:project]
    option_flags, packages = args.partition{|a| a =~ /^--/ }
    
    # Get the specified package
    # FIXME this only gets the 'Data' model, which is not necessarily what you want to get.
    model = project.getModel
    # Set any stereotype options from arguments
    set_stereotype_options(option_flags, project, model, 'generation_options')
    
    nested_packages = model.getNestedPackage
    # If passed package arg is "Data", assume that top level Data Model is meant.
    # Otherwise, look for packages in direct children of Data
    if packages.count == 1 && packages.first == "Data"
      md_packages = [model]
    else
      md_packages = packages.collect{|package| nested_packages.find{|p| p.getName == package} }
    end
    raise "Couldn't find package #{package}!" unless md_packages.any?
    
    # Select md_packages in containment tree
    browser = app.getMainFrame.getBrowser
    containment_tree = browser.getContainmentTree
    md_packages.each do |md_package|
      containment_tree.openNode(md_package, select=true, appendSelection=true, requestFocus=true, scrollToVisible=false)
    end
    
    # How to navigate and use nodes:
    # project_node = containment_tree.getRootNode
    # data_node = project_node.children.first
    # md_package_node = data_node.children.find{|c| c.getUserObject.getName == package}
    
    # How to export diagrams
    # diagrams = app.getProject.getDiagrams
    # diagrams.each do |d|
    #   System.out.println "d.inspect: #{d.inspect}"
    #   System.out.println "Found diagram: #{d.getHumanName}"
    #   System.out.println " with id: #{d.getID}"
    #   diagramFile = java.io.File.new(File.join('/Users/sdana/tmp', d.getHumanName + d.getID + '.png'))
    #   ImageExporter.export(d, ImageExporter::PNG, diagramFile)
    # end
    
    # Trigger the plugin action
    # actions_menu_creator = com.nomagic.magicdraw.actions.MenuCreatorFactory.getMenuCreator
    ap = com.nomagic.magicdraw.actions.ActionsProvider.getInstance
    actions_manager = ap.getContainmentBrowserContextActions(containment_tree)
    actions = actions_manager.getAllActions
    action_name = 'Generate Ruby UML Metamodel'
    generate_action = actions.find{|a| a.getName == action_name}
    raise "Could not find context action #{action_name}!\nAvailable actions: #{actions.collect{|a|a.getName}.inspect}" unless generate_action
    generate_action.actionPerformed(nil)
  end
  
  # Given a list of arguments, the MD project, the package that the options stereotye is applied to, and
  # the name of the options stereotype, sets all flag arguments as tags on the applied stereotype.
  # If the specified stereotype is not found, it is created under the specified package.
  # If any specified tags are not found, they are created.
  def set_stereotype_options(arguments, project, package, stereotype_name)
    stereotype = package.get_stereotype(stereotype_name)
    # Create stereotype under package and apply it to package if it doesn't exist
    unless stereotype
      package_metaclass = StereotypesHelper.getMetaClassByName(project, "Package")
      stereotype = StereotypesHelper.createStereotype(package, stereotype_name, [package_metaclass])
      StereotypesHelper.addStereotype(package, stereotype)
    end
    arguments.each do |arg|
      if arg =~ /--(.+)=(.+)/
        tag = $1
        value = case $2
        when 'false'
          type = 'boolean'
          false
        when 'true'
          type = 'boolean'
          true
        else
          type = 'String'
          $2
        end
        slot = StereotypesHelper.getSlot(package, stereotype, tag, createSlotIfNotExists=true)
        # If slot isn't present, create an appropriate tag definition on stereotype
        # This is a convenience feature so new generation options don't always need to result in a change to the test model. -SD
        unless slot
          factory = project.getElementsFactory
          tag_def = factory.createPropertyInstance
          tag_def.setName(tag)
          tag_def.setDerived(false)
          tag_def.setUnique(false)
          tag_def.setOwner(stereotype)
          tag_type = ModelHelper.findDataTypeFor(project, tag, nil)
          tag_def.setType(tag_type)
          slot = StereotypesHelper.getSlot(package, stereotype, tag, createSlotIfNotExists=true)
        end
        StereotypesHelper.setSlotValue(slot, value, appendValues=false, setAnotherEnd=false)
      end
    end
  end
end

$plugin_runner = ModelGenRunner