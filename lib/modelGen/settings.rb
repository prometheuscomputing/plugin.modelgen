$DEBUG   = true
Infinity = Float::INFINITY

load 'umm_adapter/magicdraw_extensions.rb'
class ModelTranslator
  # can't be a constant because these things are nil until after a project is loaded and plugins will load before the project.
  # EXCLUDED_PACKAGES = [MagicDraw.root_model, MagicDraw.uml_standard_profile, MagicDraw.uml_metamodel_adapter]
  def self.excluded_packages
    [MagicDraw.root_model, MagicDraw.uml_standard_profile, MagicDraw.uml_metamodel_adapter, MagicDrawAuxiliary.uml_package]
  end
end