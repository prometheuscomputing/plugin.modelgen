# MagicDraw defines a bunch of stereotypes which have these as metaclasses.  They are not defined by model_metadata/metamodel so we are monkeypatching them in here.
# require_relative 'magicdraw_specific_metamodel_classes.rb'

class ModelTranslator
  def setup_md_profile_stereotypes
    # setup additional stereotypes from the MagicDraw Profile
    md_stereotypes = MagicDraw.md_profile.getOwnedStereotype
    md_stereotypes.each do |md_stereotype| 
      md_profile_stereotypes[md_stereotype] = make_stereotype(md_stereotype, true)
    end
  end

  def get_top_level_element element
    if o = element.getOwner
      get_top_level_element o
    else
      element
    end
  end
  
  # FIXME implement documentation gathering
  def documentation md_element
    
  end
  
  def version_from_filename(filename)
    # Using semver.org versioning rules
    v = filename.slice(/\d+\.\d+\.\d+(-[a-zA-Z0-9\.]+)?/)
    unless v
      v = filename.slice(/\d+\.\d+(-[a-zA-Z0-9\.]+)?/)
      v = v + '.0' if v
    end
    unless v
      v = filename.slice(/\d+(-[a-zA-Z0-9\.]+)?/)
      v = v + '.0.0' if v
    end
    v
  end
  
  def name_from_filename(filename)
    # try to get rid of any version indications
    name = filename.split(/_?(v|V)?\d+/).first
    # Remove trailing whitespace, underscores, periods, hyphens
    name.gsub(/[_\-\.\s]*$/, '')
  end
  
  def magicdraw_filename(md_model)
    MagicDraw.filename(md_model)
  end
end