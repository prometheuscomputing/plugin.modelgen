# debugging aid
require 'rainbow'
require 'uml_metamodel'

require 'rspec'
# require 'simplecov'
require 'Foundation/load_path_management'
$:.unshift relative('../lib/')
$:.unshift relative('../../magicdraw_plugin_runner/lib/')
$:.unshift relative('../../uml_metamodel/lib/')
require 'magicdraw_plugin_runner'

# Enable old syntax for Rspec 3.0
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

def setup_dsl_file_path(filename)
  code_dir       = File.expand_path('~/Prometheus/Generated_Code/')
  @filename_base = filename
  @dsl_path      = File.join(code_dir, @filename_base + '.rb')
  # Remove it if it exists
  FileUtils.rm(@dsl_path) if File.exist?(@dsl_path)
end

# This doesn't work since magicdraw plugin runner creates a new JRuby instance that won't be covered
# TODO: implement SimpleCov coverage checks when running magicdraw_plugin_runner
# SimpleCov.start do
#   add_filter 'spec/'
#   add_filter 'tmp/'
#   add_filter 'test_data/'
# end