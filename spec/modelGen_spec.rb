require 'spec_helper'
require 'fileutils'
require 'Foundation/load_path_management'
require 'indentation'

# NOTE: when debugging these tests, $output may be printed to see any errors that occurred during plugin execution via: puts $output

describe 'modelGen' do
  before :all do
    @model_gen_runner_path = relative('../lib/modelGen/model_gen_runner.rb')
    @source_model_file     = 'ERR-V2.0.xml'
    @source_model_path     = relative("../test_data/#{@source_model_file}")
    @expected_message = <<-HEREDOC
Translation succeeded with warnings:
 -- Not including stereotype tag: UML Standard Profile::MagicDraw Profile::Info::version with value '"0.3.8"' applied on Gui_Builder_Profile
 -- Not including stereotype tag: UML Standard Profile::MagicDraw Profile::typeModifier::typeModifier with value '"null"' applied on XML Schema::XML Schema::id_suffix_for_references
HEREDOC
  end
  
  # TODO: replace this context with CarExample
  context "Generating ElectionResults model" do
    before :all do
      setup_dsl_file_path('ERR-2.0.0')
      @message = MagicdrawPluginRunner.run(@model_gen_runner_path, [@source_model_path, 'Data',
        '--pluralize_role_names=true', '--snakecase_role_names=true' # Override project-specific setting with normal default behavior
      ])
      # Ensure that DSL successfully generated
      expect(File.exist?(@dsl_path)).to be true
      # Read the resulting UML Metamodel from the DSL
      @project = UmlMetamodel.from_dsl_file(@dsl_path)
    end
  
    it "should not produce any errors" do
      expect(@message).not_to include('Error')
    end
    
    it "should not produce any unexpected warnings" do
      # expect(@message).not_to include('Succeeded with warnings:')
      expect(@message).to include(@expected_message.strip)
    end
    
    it "should generate a top-level project wrapper" do
      @project.should be_a(UmlMetamodel::Project)
      @project.original_filename.should == @source_model_file
    end
    
    it "should generate documentation from the model for the various components" do
      # Test Package documentation
      # No examples
      
      # Test Classifier (Class) documentation
      election_report = UmlMetamodel.find('ElectionResults::ElectionReport', @project)
      election_report.documentation.should == 'The root class/element; attributes pertain to the status and format of the report and when generated.'
      
      # Test Property documentation
      election_report_generated_date = UmlMetamodel.find('ElectionResults::ElectionReport::GeneratedDate', @project)
      election_report_generated_date.documentation.should == 'Identifies the time that the election report was generated.'
      
      # Test Association documentation
      ballot_style_party_assoc = UmlMetamodel.find('ElectionResults::BallotStyle::Party', @project).association
      ballot_style_party_assoc.documentation.should == 'This association is populated if the BallotStyle is specific to a particular party or parties'
      
      # Test Enumeration Literal documentation
      advanced_to_runoff_literal = UmlMetamodel.find('ElectionResults::CandidatePostElectionStatus::advanced-to-runoff', @project)
      advanced_to_runoff_literal.documentation.should == 'For ranked order voting.'
      
      # Note: This stereotype was removed in Gui_Builder_Profile v3.21
      # Test Stereotype documentation
      nil_for_root = UmlMetamodel.find('Gui_Builder_Profile::nil_for_root', @project)
      nil_for_root.documentation.should == 'When specified on a property or association of a <<root>> Class, causes any Class of that type to be considered non-root if the <<nil_for_root>> association or property is set.'
      
      # Test Tag documentation
      # No examples
      
      # Test Applied Stereotype documentation
      # No examples
      
      # Test Applied Tag documentation
      # No examples, plus this doesn't currently work
    end
    
    it "should not name associations with unspecified role names" do
      election_report = UmlMetamodel.find('ElectionResults::ElectionReport', @project)
      election_report.properties.find{|p| p.type.name == 'Election'}.name.should be_nil
    end
    
    it "should generate all top level packages except for Uml Standard Profile and UmlMetamodelAdapter" do
      package_names = @project.packages.collect{|p|p.name}
      package_names.should =~ ["Application", "ElectionResults", "Gui_Builder_Profile", "Primitives (Class based)", "XML Schema", "Xmldsig"]
    end
  end
  
  context "Generating ElectionResults model without using Data package, with pluralize_role_names and snakecase_role_names turned off" do
    before :all do
      setup_dsl_file_path('ERR-2.0.0')
      @message = MagicdrawPluginRunner.run(@model_gen_runner_path, [@source_model_path, 
        'ElectionResults', 'Gui_Builder_Profile', 'Primitives (Class based)', 'Xmldsig', 'XML Schema'
      ])
      # Ensure that DSL successfully generated
      expect(File.exist?(@dsl_path)).to be true
      # Read the resulting DSL into a string
      @dsl = File.read(@dsl_path)
      # Read the resulting UML Metamodel from the DSL
      @project = UmlMetamodel.from_dsl_file(@dsl_path)
    end
  
    it "should not produce any errors or unexpected warnings" do
      expect(@message).not_to include('Error')
      # expect(@message).not_to include('Succeeded with warnings:')
      expect(@message).to include(@expected_message.strip)
    end
    
    it "should generate a top-level project wrapper with specified packages as contents" do
      @project.should be_a(UmlMetamodel::Project)
      @project.original_filename.should == @source_model_file
      @project.contents.count.should == 5
      # Check that there are not multiple definitions for a class
      @dsl.scan(/klass "BallotSelection"/).count.should == 1
    end
    
    it "should generate all specified packages" do
      package_names = @project.packages.collect{|p|p.name}
      package_names.should =~ ["ElectionResults", "Gui_Builder_Profile", "Primitives (Class based)", "XML Schema", "Xmldsig"]
    end
    
    # TODO: add an example where this occurs (it doesn't in the ERR-V2.0 example)
    # it "should differentiate duplicate property names (or absence thereof) by appending inverse role name or type name" do
    # end
  end
  
  context "Generating ElectionResults model by selecting a model that imports the relevant packages " do
    before :all do
      setup_dsl_file_path('ERR Application-0.0.0')
      @message = MagicdrawPluginRunner.run(@model_gen_runner_path, [@source_model_path, 'Application'])
      # Ensure that DSL successfully generated
      expect(File.exist?(@dsl_path)).to be true
      # Read the resulting UML Metamodel from the DSL
      @project = UmlMetamodel.from_dsl_file(@dsl_path)
    end
    
    it "should not produce any errors" do
      expect(@message).not_to include('Error')
    end
    
    it "should not produce any unexpected warnings" do
      puts @message
      # expect(@message).not_to include('Succeeded with warnings:')
      expect(@message).to include(@expected_message.strip)
    end
    
    it "should generate a top-level project wrapper" do
      @project.should be_a(UmlMetamodel::Project)
      @project.original_filename.should == @source_model_file
      @project.version.should == '0.0.0'
      @project.name.should == 'ERR Application'
    end
    
    it "should generate all top level packages except for Uml Standard Profile and UmlMetamodelAdapter" do
      package_names = @project.packages.collect{|p|p.name}
      package_names.should =~ ["Application", "ElectionResults", "Gui_Builder_Profile", "Primitives (Class based)", "XML Schema", "Xmldsig"]
    end
  end
end

describe 'modelGen' do
  before :all do
    @model_gen_runner_path = relative('../lib/modelGen/model_gen_runner.rb')
    @source_model_file     = 'car_example_2.5.0.mdzip'
    @source_model_path     = relative("../test_data/#{@source_model_file}")
  end
  
  context "Generating CarExample model" do
    before :all do
      setup_dsl_file_path('CarExample-2.5.0')
      @message = MagicdrawPluginRunner.run(@model_gen_runner_path, [@source_model_path, 'Application'])
      # Ensure that DSL successfully generated
      expect(File.exist?(@dsl_path)).to be true
      # Read the resulting UML Metamodel from the DSL
      # @project = UmlMetamodel.from_dsl_file(@dsl_path)
      @expected_message = <<-HEREDOC
Translation succeeded with warnings:
 -- Not including stereotype tag: UML Standard Profile::MagicDraw Profile::Info::version with value '"0.3.21"' applied on Gui_Builder_Profile
HEREDOC
    end
  
    it "should not produce any errors" do
      puts @message
      expect(@message).not_to include('Error')
    end
    
    it "should not produce any unexpected warnings" do
      expect(@message.strip).to eq(@expected_message.strip)
    end
  end
end